import { combineReducers } from 'redux';

import panel from './panel';
import deprecations from './deprecations';
import highlighting from './highlighting';

export default combineReducers({ panel, deprecations, highlighting });
