import { STORE_DEPRECATION, CLEAR_DEPRECATIONS, FILTER_DEPRECATIONS } from '../constants/deprecations';

export const storeDeprecation = deprecation => ({
  type: STORE_DEPRECATION,
  deprecation
});

export const clearDeprecations = () => ({
  type: CLEAR_DEPRECATIONS
});

export const filterDeprecations = filterQuery => ({
  type: FILTER_DEPRECATIONS,
  filterQuery
});
