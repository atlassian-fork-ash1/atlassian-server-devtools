import { emit } from '../../portConnection';
import {
  HIGHLIGHT_NODE,
  UNHIGHLIGHT_NODE,
  SCROLL_TO_NODE,
  TOGGLE_HIGHLIGHTING_NODES,
  REMOVE_HIGHLIGHTING_ON_ALL_NODES
} from '../contants';

export const highlightNode = nodeId => (
  emit(HIGHLIGHT_NODE, { nodeId })
);

export const unhighlightNode = nodeId => (
  emit(UNHIGHLIGHT_NODE, { nodeId })
);

export const scrollToNode = nodeId => (
  emit(SCROLL_TO_NODE, { nodeId })
);

export const toggleHighlightNodes = highlight => (
  emit(TOGGLE_HIGHLIGHTING_NODES, { highlight })
);

export const removeHighlightingOnAllNodes = () => (
  emit(REMOVE_HIGHLIGHTING_ON_ALL_NODES)
);
