import React from 'react';
import PropTypes from 'prop-types';
import styles from './Sidebar.less';

const Sidebar = ({ children }) => (
  <div className={styles.Sidebar}>
    <ol className={styles.SidebarActionsList}>
      {children}
    </ol>
  </div>
);

Sidebar.propTypes = {
  children: PropTypes.node.isRequired
};

export default Sidebar;
