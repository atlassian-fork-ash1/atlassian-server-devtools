import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DOMInspector, chromeLight } from 'react-inspector';
import classNamesBind from 'classnames/bind';
import * as semver from 'semver';

import ConsoleMessage from '../Console/ConsoleMessage';
import DeprecationPropType from '../../propTypes/DeprecationPropType';
import nodeDehydrate from '../../helpers/nodeDehydrate';
import { getAuiUpgradeGuideLink } from '../../helpers/links';

import styles from './DeprecationMessage.less';

const classNames = classNamesBind.bind(styles);

const isVersionDeprecated = (version, auiVersion) => (
  auiVersion ? semver.gt(auiVersion, version) : false
);

const getDeprecationLevel = isDeprecated => ({
  isError: isDeprecated,
  isWarning: !isDeprecated
});

class DeprecationMessage extends Component {
  constructor() {
    super();

    this.state = {
      isHighlighted: false
    };

    this.highlightNode = this.highlightNode.bind(this);
    this.unhighlightNode = this.unhighlightNode.bind(this);
  }

  highlightNode(nodeId) {
    this.setState({
      isHighlighted: true
    });

    this.props.onHighlight(nodeId);
  }

  unhighlightNode(nodeId) {
    this.setState({
      isHighlighted: false
    });

    this.props.onUnhighlight(nodeId);
  }

  render() {
    const { onElementClick, deprecation, showTimestamps } = this.props;
    const {
      timestamp,
      node,
      nodeId,
      selector,
      displayName,
      auiVersion,
      version,
      alternativeName,
      extraInfo,
      removeVersion
    } = deprecation;

    const { isHighlighted } = this.state;

    const isDeprecated = isVersionDeprecated(version, auiVersion);

    const anchor = (
      <a rel="noopener noreferrer" target="_blank" href={getAuiUpgradeGuideLink(version)}>
        {isDeprecated ? `Deprecated in ${version}` : `Will be deprecated in ${version}`}
      </a>
    );

    const message =
      `${displayName ? `[${displayName}]` : ''} Using deprecated selector "${selector}".
       ${extraInfo || ''}
       ${alternativeName ? ` Use the ${alternativeName} pattern instead.` : ''}
       ${removeVersion ? ` The pattern will be removed in ${removeVersion} version of AUI.` : ''}
       `;

    const className = classNames(styles.DeprecationMessage, {
      [styles.NodeIsHighlighted]: isHighlighted
    });

    const errorLevelProps = getDeprecationLevel(isDeprecated);

    return (
      <ConsoleMessage
        showTimestamps={showTimestamps}
        timestamp={timestamp}
        anchor={anchor}
        {...errorLevelProps}
        className={className}
      >
        <div>{message}</div>

        <div
          className={styles.InspectorWrapper}
          onClick={() => onElementClick(nodeId)}
          onMouseEnter={() => this.highlightNode(nodeId)}
          onMouseLeave={() => this.unhighlightNode(nodeId)}
        >
          <DOMInspector
            theme={{ ...chromeLight, BASE_BACKGROUND_COLOR: 'transparent' }}
            data={nodeDehydrate(node)}
          />
        </div>
      </ConsoleMessage>
    );
  }
}

DeprecationMessage.propTypes = {
  onHighlight: PropTypes.func.isRequired,

  onUnhighlight: PropTypes.func.isRequired,

  onElementClick: PropTypes.func.isRequired,

  showTimestamps: PropTypes.bool,

  // eslint-disable-next-line react/no-typos
  deprecation: DeprecationPropType.isRequired
};

DeprecationMessage.defaultProps = {
  showTimestamps: false
};

export default DeprecationMessage;
